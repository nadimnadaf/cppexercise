/*
 * @auther Nadim Nadaf
 * @date 24 April 2022
 * @subject Write a c++ program to swap two variables using function (Dont use pointer)
 * or
 * @subject C++ program to swap two numbers using friend function
 * */


#include<iostream>

using namespace std;

class swap_class{
	/* Declaration of variables of swap class*/
	int var1, var2, temp;
	public:
	/* Define the parameterised constructor, for input */
	swap_class(int a, int b){
		this->var1 = a;
		this->var2 = b;
	}
	/* Declare a friend function to swap, take arg as call by referance */
	friend void swap(swap_class&);
};
/* Define the swap function outside the class*/
void swap(swap_class& t1){
	/* call by referance is used to passed object copy to function */
	cout << "Before Swapping : " << t1.var1 << " " <<t1.var2;

	/* Swap Operation with swap class variables  */
	t1.temp = t1.var1;
	t1.var1 = t1.var2;
	t1.var2 = t1.temp;
	cout << "\nAfter Swapping : " << t1.var1 << " " << t1.var2 << "\n";
}

/* Main Driver Code */
int main(){
	/* Declare & Initialized the swap object */
	swap_class s1(10, 20);
	swap(s1);
}

